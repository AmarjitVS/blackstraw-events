import 'package:flutter/material.dart';

class Colors {

  const Colors();

  static const Color appBarTitle = const Color(0xFFFFFFFF);
  static const Color appBarIconColor = const Color(0xFFFFFFFF);
  static const Color appBarDetailBackground = const Color(0x00FFFFFF);
  static const Color appBarGradientStart = const Color(0xFF3383FC);
  static const Color appBarGradientEnd = const Color(0xFF00C6FF);

  //static const Color programCard = const Color(0xFF434273);
  static const Color programCard = const Color.fromRGBO(10, 57, 76, 1);
  static const Color bottomBarCircleColor = const Color.fromRGBO(59, 76, 163, 1);
  static const Color contactCard = const Color.fromRGBO(196, 11, 64, 1);
  static const Color bottomBarBackground = const Color.fromRGBO(0, 204, 255, 1);
  static const Color bottomBarTextColor = const Color.fromRGBO(0, 0, 0, 1);
//  static const Color programCard = const Color(0xFF8685E5);
  //static const Color programListBackground = const Color(0xFF3E3963);
  static const Color programPageBackground = const Color.fromRGBO(110, 85, 124, 1);
//  static const Color programPageBackground = const Color(0xFF736AB7);
  static const Color programTitle = const Color(0xFFFFFFFF);
//  static const Color programShortDescription = const Color(0x66FFFFFF);
//  static const Color programDateTime = const Color(0x66FFFFFF);
  static const Color programShortDescription = const Color.fromRGBO(227, 227, 227, 1);
  static const Color programDateTime = const Color.fromRGBO(227, 227, 227, 1);

}

Color hexToColor(String hexString, {String alphaChannel = 'FF'}) {
  return Color(int.parse(hexString.replaceFirst('#', '0x$alphaChannel')));
}

class Dimens {
  const Dimens();

  static const programWidth = 100.0;
  static const programHeight = 100.0;
}

class TextStyles {

  const TextStyles();

  static const TextStyle appBarTitle = const TextStyle(
    color: Colors.appBarTitle,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    fontSize: 26.0
  );

  static const TextStyle developerMark = const TextStyle(
    color: Color(0xff000000),
    fontSize: 14.0,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
  );

  static const TextStyle programTitle = const TextStyle(
    color: Colors.programTitle,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w600,
    fontSize: 20.0
  );

  static const TextStyle programShortDescription = const TextStyle(
    color: Colors.programShortDescription,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w300,
    fontSize: 12.0
  );

  static const TextStyle programDateTime = const TextStyle(
    color: Colors.programDateTime,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w300,
    fontSize: 12.0
  );

  static const TextStyle contactStyle = const TextStyle(
    color: Colors.programDateTime,
    fontFamily: 'Poppins',
    fontWeight: FontWeight.w300,
    fontSize: 16.0
  );


}
