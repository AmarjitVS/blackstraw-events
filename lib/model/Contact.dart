class Contact {
  final String id;
  final String name;
  final String incharge;
  final String contact;
  final String image;

  const Contact({this.id, this.name, this.incharge, this.contact, this.image});

}
