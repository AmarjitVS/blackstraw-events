class Program {
  final String id;
  final String name;
  final String date;
  final String time;
  final String shortDescription;
  final String image;

  const Program({this.shortDescription,
      this.id, this.name, this.date, this.time, this.image});
}
