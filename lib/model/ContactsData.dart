import 'Contact.dart';

class ContactsData {
  static final List<Contact> contacts = [
    const Contact(
        id: "1",
        name: "Priya",
        contact: "+91 9600034464",
        incharge: "Resort Arrangements & Culturals",
        image: "assets/img/moon.png"),
    const Contact(
        id: "2",
        name: "Karthik Venkatesh",
        contact: "+91 9080310113",
        incharge: "Resort Arrangements & Culturals",
        image: "assets/img/moon.png"),
    const Contact(
        id: "3",
        name: "Munirathinam",
        contact: "+91 9944667693",
        incharge: "Food & Water",
        image: "assets/img/moon.png"),
    const Contact(
        id: "4",
        name: "Sabith",
        contact: "+91 7418908110",
        incharge: "Travel",
        image: "assets/img/moon.png"),
  ];

  static Contact getContactById(id) {
    return contacts.where((p) => p.id == id).first;
  }
}
