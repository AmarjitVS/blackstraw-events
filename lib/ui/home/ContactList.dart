import 'package:blackstraw/model/Contact.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'ContactRow.dart';

class ContactList extends StatelessWidget {
  Center _buildLoadingIndicator() {
    return Center(
      child: new CircularProgressIndicator(),
    );
  }

  @override
  Widget build(BuildContext context) {
    String db = "Contacts";
    return Expanded(
        child: StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection(db).orderBy('id').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) return _buildLoadingIndicator();
        final int transactionCount = snapshot.data.documents.length;
        return ListView.builder(
            itemCount: transactionCount,
            itemBuilder: (_, int index) {
              final DocumentSnapshot data = snapshot.data.documents[index];
              return new ContactRow(new Contact(
                id: data['id'].toString(),
                name: data['name'],
                incharge: data['incharge'],
                contact: data['contact'],
                image: data['image'],
              ));
            });
      },
    ));
  }
//  @override
//  Widget build(BuildContext context) {
//    return new Flexible(
//      child: new Container(
//        child: new ListView.builder(
//          itemExtent: 160.0,
//          itemCount: ContactData.contacts.length,
//          itemBuilder: (_, index) =>
//          new ContactRow(ContactData.contacts[index]),
//        ),
//      ),
//    );
//  }
}
