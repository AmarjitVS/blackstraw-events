import 'package:blackstraw/model/Contact.dart';
import 'package:flutter/material.dart';
import 'package:blackstraw/Theme.dart' as Theme;
import 'package:url_launcher/url_launcher.dart';
import 'package:toast/toast.dart';

class ContactRow extends StatelessWidget {
  final Contact contact;

  ContactRow(this.contact);

  _launchCaller(BuildContext context, final String contactNumber) async {
    try {
      if (await canLaunch(contactNumber)) {
        await launch(contactNumber);
      } else {
        throw 'Could not launch $contactNumber';
      }
    } catch (Exception) {
      Toast.show("Could not launch dialer", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
    }
  }

  @override
  Widget build(BuildContext context) {
    final contactThumbnail = new Container(
      alignment: new FractionalOffset(0.0, 0.5),
      child: new Hero(
        tag: 'contact-icon-${contact.id}',
        child: new Image(
          image: new AssetImage(contact.image),
          height: Theme.Dimens.programHeight,
          width: Theme.Dimens.programWidth,
        ),
      ),
    );

    final contactCard = new Container(
      margin: const EdgeInsets.only(left: 48.0, right: 0.0),
      decoration: new BoxDecoration(
        color: Theme.Colors.contactCard,
        shape: BoxShape.rectangle,
        borderRadius: new BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
              color: Colors.black,
              blurRadius: 10.0,
              offset: new Offset(0.0, 10.0))
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.only(bottom:15.0),
        child: new Container(
          margin: const EdgeInsets.only(top: 16.0, left: 53.0),
//        constraints: new BoxConstraints.expand(),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(right: 15.0),
                child: new Text(contact.name,
                    maxLines: 2,
                    textAlign: TextAlign.justify,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.TextStyles.programTitle),
              ),
              Container(
                padding: EdgeInsets.only(right: 15.0, top: 5.0, left: 10.0),
                child: new Text(contact.incharge,
                    maxLines: 2,
                    textAlign: TextAlign.justify,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.TextStyles.programShortDescription),
              ),
              new Container(
                  color: const Color(0xFF00C6FF),
                  width: 24.0,
                  height: 1.0,
                  margin: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 10.0)),
              SizedBox(
                height: 5.0,
              ),
              Container(
                padding: EdgeInsets.only(left: 10.0),
                child: new Row(
                  children: <Widget>[
                    new Icon(Icons.perm_contact_calendar,
                        size: 20.0, color: Theme.Colors.programDateTime),
                    new SizedBox(
                      width: 4.0,
                    ),
                    new Text(contact.contact,
                        style: Theme.TextStyles.contactStyle),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );

    return new Container(
      margin: const EdgeInsets.only(top: 8.0, bottom: 16.0),
      child: new FlatButton(
        onPressed: () =>
            _launchCaller(context, 'tel://' + this.contact.contact),
        child: new Stack(
          children: <Widget>[
            contactCard,
            contactThumbnail,
          ],
        ),
      ),
    );
  }
}
