import 'dart:ui';

import 'package:flutter/material.dart';

import 'ContactList.dart';
import 'GradientAppBar.dart';

class ContactsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        new Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new ExactAssetImage('assets/img/back.png'),
              fit: BoxFit.cover,
            ),
          ),
        ),
        new Column(
          children: <Widget>[
            new GradientAppBar("Blackstraw"),
            new ContactList(),
          ],
        ),
      ],
    );
  }
}
