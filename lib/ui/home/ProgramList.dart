import 'package:blackstraw/model/Program.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'ProgramRow.dart';

class ProgramList extends StatelessWidget {
  Center _buildLoadingIndicator() {
    return Center(
      child: new CircularProgressIndicator(),
    );
  }

  @override
  Widget build(BuildContext context) {
    String db = "Programs";
    return Expanded(
        child: StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection(db).orderBy("id").snapshots(),
      builder:
          (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) return _buildLoadingIndicator();
        final int transactionCount = snapshot.data.documents.length;
        return ListView.builder(
            itemCount: transactionCount,
            itemBuilder: (_, int index) {
              final DocumentSnapshot data = snapshot.data.documents[index];
              return new ProgramRow(new Program(
                  id: data['id'].toString(),
                  name: data['name'],
                  image: data['image'],
                  date: data['date'],
                  shortDescription: data['shortDescription'],
                  time: data['time']));
            });
      },
    ));
  }
//  @override
//  Widget build(BuildContext context) {
//    return new Flexible(
//      child: new Container(
//        child: new ListView.builder(
//          itemExtent: 160.0,
//          itemCount: ProgramData.programs.length,
//          itemBuilder: (_, index) =>
//              new ProgramRow(ProgramData.programs[index]),
//        ),
//      ),
//    );
//  }
}
