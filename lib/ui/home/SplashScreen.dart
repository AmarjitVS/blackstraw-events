import 'package:flutter/material.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:blackstraw/Theme.dart' as Theme;

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _AnimatedLiquidCircularProgressIndicator(),
        ],
      ),
    );
  }
}

class _AnimatedLiquidCircularProgressIndicator extends StatefulWidget {
  @override
  State<StatefulWidget> createState() =>
      _AnimatedLiquidCircularProgressIndicatorState();
}

class _AnimatedLiquidCircularProgressIndicatorState
    extends State<_AnimatedLiquidCircularProgressIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 5),
    );

    _animationController.addListener(() => setState(() {
          if (_animationController.value * 100 >= 99.0) {
            _animationController.dispose();
            Navigator.of(context).pushReplacementNamed("/home");
          }
        }));
    _animationController.repeat();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final percentage = (_animationController.value * 100) + 1;
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: LiquidLinearProgressIndicator(
            value: _animationController.value,
            valueColor: AlwaysStoppedAnimation(Colors.red),
            backgroundColor: Colors.white,
            direction: Axis.vertical,
          ),
        ),
        new Column(
          children: <Widget>[
            Image(
              image: new AssetImage("assets/img/BlackstrawLogo.png"),
              height: MediaQuery.of(context).size.width / 3,
              width: MediaQuery.of(context).size.width / 3,
            ),
            SizedBox(
              height: 10.0,
            ),
            Text(
              "${percentage.toStringAsFixed(0)}%",
              style: TextStyle(
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        Positioned(
          bottom: 15.0,
          child: Text(
            "Developed by \"THE BOYS HOSTEL\"",
            style: Theme.TextStyles.developerMark,
          ),
        )
      ],
    );
  }
}
