import 'package:blackstraw/model/Program.dart';
import 'package:flutter/material.dart';
import 'package:blackstraw/Theme.dart' as Theme;

class ProgramRow extends StatelessWidget {
  final Program program;

  ProgramRow(this.program);

  @override
  Widget build(BuildContext context) {
    final programThumbnail = new Container(
      alignment: new FractionalOffset(0.0, 0.5),
      child: new Hero(
        tag: 'program-icon-${program.id}',
        child: new Image(
          image: new AssetImage(program.image),
          height: Theme.Dimens.programHeight,
          width: Theme.Dimens.programWidth,
        ),
      ),
    );

    final programCard = new Container(
      margin: const EdgeInsets.only(left: 48.0, right: 0.0),
      decoration: new BoxDecoration(
        color: Theme.Colors.programCard,
        shape: BoxShape.rectangle,
        borderRadius: new BorderRadius.circular(8.0),
        boxShadow: <BoxShadow>[
          new BoxShadow(
              color: Colors.black,
              blurRadius: 10.0,
              offset: new Offset(0.0, 10.0))
        ],
      ),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 10.0),
        child: new Container(
          margin: const EdgeInsets.only(top: 16.0, left: 53.0),
//          constraints: new BoxConstraints.expand(),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: new Text(program.name,
                    maxLines: 2,
                    textAlign: TextAlign.justify,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.TextStyles.programTitle),
              ),
              Container(
                padding: EdgeInsets.only(right: 15.0, top: 5.0, left: 10.0),
                child: new Text(program.shortDescription,
                    maxLines: 2,
                    textAlign: TextAlign.justify,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.TextStyles.programShortDescription),
              ),
              Container(
                  color: const Color(0xFF00C6FF),
                  width: 24.0,
                  height: 1.0,
                  margin: const EdgeInsets.symmetric(
                      vertical: 8.0, horizontal: 10.0)),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Icon(Icons.date_range,
                            size: 14.0, color: Theme.Colors.programDateTime),
                        new SizedBox(
                          width: 8.0,
                        ),
                        new Text(program.date,
                            style: Theme.TextStyles.programDateTime),
                      ],
                    ),
                    new SizedBox(
                      height: 5.0,
                    ),
                    new Row(
                      children: <Widget>[
                        new Icon(Icons.access_time,
                            size: 14.0, color: Theme.Colors.programDateTime),
                        new SizedBox(
                          width: 8.0,
                        ),
                        new Text(program.time,
                            style: Theme.TextStyles.programDateTime),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );

    return new Container(
      margin: const EdgeInsets.only(top: 8.0, bottom: 16.0),
      child: new FlatButton(
        onPressed: null,
        child: new Stack(
          children: <Widget>[
            programCard,
            programThumbnail,
          ],
        ),
      ),
    );
  }
}
