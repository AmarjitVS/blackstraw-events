import 'package:blackstraw/ui/home/ContactPage.dart';
import 'package:blackstraw/ui/home/HomePage.dart';
import 'package:blackstraw/ui/home/SplashScreen.dart';
import 'package:flutter/material.dart';

void main() {
  var routes = {
    '/splashScreen': (context) => new SplashScreen(),
    '/home': (context) => new HomePage(),
    '/homePage': (context) => new HomePageBody(),
    '/conatctsPage': (context) => new ContactsPage(),
  };
  runApp(new MaterialApp(
    title: 'Blackstraw',
    home: new SplashScreen(),
    routes: routes,
    debugShowCheckedModeBanner: false,
  ));
}
